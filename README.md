# KAMEL Integration In ![alt text](https://cdn.iconscout.com/public/images/icon/free/png-128/docker-logo-brand-development-tools-34129264099f3189-128x128.png "Docker Logo")

## Sypnosis

The purpose of this project is to test the different services part of KAMEL.
In order to control the environment and dependencies we use docker.
Services tested :

* lanmngmt_service

* redfish_server

* service_registry

* zmqlib

## Install docker

On Ubuntu 16.04 simply follow this guide :

[Docker-CE Guide](https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce)

## Build the docker image

Checkout this project and go to the main directory were 'Dockerfile' is contained.
Checkout submodules and checkout the develop branch.

```bash
git submodule update --init
git submodule foreach git checkout develop
```

Build the container

```bash
sudo docker build -t allservices .
```

## Run the docker image

```bash
sudo docker run -v /dev/log:/dev/log --name='KAMEL' --hostname='KAMEL' -t -d -p 4430:443 allservices:latest
```

## Example of RedFish request

```bash
curl https://127.0.0.1:4430/redfish -k
```

## Developing/testing with branches

All services are added as git submodules.
You can easily specify the branch you want for any of the module in the .gitmodules file.

## Optional - Install Portainer (WebGUI for Docker)

Portainer is a container that will help you manage other containers through a web UI.
Following this procedure will make Portainer start on boot.
Through Portainer you can manage which container is running and the ressource usage.

```bash
sudo docker volume create portainer_data
sudo docker run -d -p 9000:9000 --restart always --name portainer \
-v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer
```

You can log to Portainer at the following address : [http://127.0.0.1:9000](http://127.0.0.1:9000)
Setup your account and add your local system to the hosts to monitor.
