FROM python:2.7
RUN mkdir /code
RUN mkdir /etc/kamel/
COPY . /code
WORKDIR /code
RUN apt update
RUN apt install vim -y
RUN cp -r redfish_server/redfishservice/config /etc/kamel/
RUN cp -r redfish_server/redfishservice/redfishservice/model /etc/kamel
RUN cp -r lanmngmt_service/lanmanagement/config /etc/kamel/
RUN cp -r service_registry/serviceregistry/config /etc/kamel
RUN cp -r boardinv_service/boardinventory/conf/XML /etc/kamel
RUN cp -r boardinv_service/boardinventory/conf/. /etc/kamel/config/

RUN pip install -r requirements.txt
RUN for i in $(find . -print | grep -i 'setup.py' | sed 's|setup.py||'); do cd $i; python setup.py install; cd /code; done

EXPOSE 443
ENTRYPOINT ["./start_services.sh"]


#SUBMODULES
#git submodule add -b develop git@bitbucket.org:kci1/lanmngmt_service.git
#git submodule add -b develop git@bitbucket.org:kci1/redfish_server.git
#git submodule add -b develop git@bitbucket.org:kci1/service_registry.git
#ENTRYPOINT ["python2", "profile.py"]
#git submodule add -b develop git@bitbucket.org:kci1/zmqlib.git
#git submodule add -b develop git@bitbucket.org:kci1/boardinv_service.git
